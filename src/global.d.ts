import React from 'react';

declare global {
  namespace JSX {
    interface Element {}
    interface IntrinsicElements {
      import: React.DetailedHTMLProps<
        React.EmbedHTMLAttributes<HTMLEmbedElement>,
        HTMLEmbedElement
      >;
    }
  }
}
