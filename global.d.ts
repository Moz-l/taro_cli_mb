import React from 'react';

declare module '*.json' {
  const value: any;
  export default value;
}
declare global {
  namespace JSX {
    interface IntrinsicElements {
      import: React.DetailedHTMLProps<
        React.EmbedHTMLAttributes<HTMLEmbedElement>,
        HTMLEmbedElement
      >;
    }
  }
}
